<?php

return [
    'newAnnouncement' => 'Create a new article',
    'allAnnouncements' => 'All articles',
    'noAnnouncements' => 'No items available',
    'searched' => 'You searched: ',
    'price' => 'Price',
    'description' => 'Description',
    'category' => 'Category',
];


?>