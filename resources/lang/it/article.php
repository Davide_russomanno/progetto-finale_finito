<?php

return [
    'newAnnouncement' => 'Crea un nuovo articolo',
    'allAnnouncements' => 'Tutti gli articoli',
    'noAnnouncements' => 'Nessun articolo disponibile',
    'searched' => 'Hai cercato: ',
    'price' => 'Prezzo',
    'description' => 'Descrizione',
    'category' => 'Categoria',
];


?>