<?php

return [
    'newAnnouncement' => 'Crear un nuevo artículo',
    'allAnnouncements' => 'Todos los artículos',
    'noAnnouncements' => 'No hay artículos disponibles',
    'searched' => 'Buscaste: ',
    'price' => 'Precio',
    'description' => 'Descripción',
    'category' => 'Categoría',
];


?>