<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Franco',
            'email' => 'franco@franco.com',
            'password' => Hash::make('123456789'),
            'img' => "img/avatar.jpg",
        ]);

        \App\Models\User::factory()->create([
            'name' => 'Pablo',
            'email' => 'pablo@pablo.com',
            'password' => Hash::make('123456789'),
            'img' => "img/boss.png",
            'is_revisor' => 1,
        ]);

        $categories = [
            ['elettronica', 'electronic', 'electronics'], 
            ['abbigliamento', 'clothing', 'ropa'], 
            ['motori', 'motors', 'motores'], 
            ['telefonia', 'phones', 'telefonìa'], 
            ['arredamento', 'home', 'muebles'], 
            ['pet', 'pet', 'animales'], 
            ['libri', 'books', 'libros'], 
            ['infanzia', 'children', 'infancias'], 
            ['sport', 'sport', 'sport'], 
            ['collezionismo', 'collecting', 'coleccionar']];
        foreach($categories as $category){
            Category::create([
                'name'=>$category[0],
                'en'=>$category[1],
                'es'=>$category[2],
            ]);
        }
    }
}
